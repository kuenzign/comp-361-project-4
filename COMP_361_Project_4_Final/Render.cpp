#include "Render.h"

Render::Render(gVector4 AT, gVector4 EYEPOS, gVector4 UP, gVector4 LCOL, gVector4 LPOS, gVector4 BCOL, float FOVY, int RESOx, int RESOy, int aaFactor)
{
	triangleP1 = gVector4(-1, 0, 0, 1);
	triangleP2 = gVector4(0, 1, 0, 1);
	triangleP3 = gVector4(1, 0, 0, 1);
	at = AT;
	eye = EYEPOS;
	up = UP;
	backgroundColor = BCOL;
	lightPosition = LPOS;
	lightColor = LCOL;
	width = RESOx;
	height = RESOy;
	antiAliasingFactor = aaFactor;
	fovY = FOVY * (float)(PI / 180);
	fovX = 2 * atanf(tanf(fovY * 0.5f) * ((float)width / (float)height));

	n = (at - eye).getNormalized();
	u = (up % n).getNormalized();
	theta = fovX / 2.0f;
	phi = fovY / 2.0f;
	//fovX = theta * 2;
	//fovY = phi * 2;
	basisV = (n % u) * tanf(phi);
	basisH = u * tanf(theta);
	basisM = eye + n;
}

Render::~Render(void)
{
}

bool Render::approxEquals(float a, float b)
{
	const float epsilon = 0.00001f;
	if (a == b)
	{
		return true;
	}
	else if (a * b == 0.0f)
	{
		return fabs(a - b) < epsilon * epsilon;
	}
	else
	{
		return fabs(a - b) / (fabs(a) + fabs(b)) < epsilon;
	}
}

double Render::triangeArea(gVector4 p1, gVector4 p2, gVector4 p3)
{
	float det1 = p1[1] * (p2[2] - p3[2]) - p1[2] * (p2[1] - p3[1]) + (p2[1] * p3[2] - p2[2] * p3[1]);
	float det2 = p1[2] * (p2[0] - p3[0]) - p1[0] * (p2[2] - p3[2]) + (p2[2] * p3[0] - p2[0] * p3[2]);
	float det3 = p1[0] * (p2[1] - p3[1]) - p1[1] * (p2[0] - p3[0]) + (p2[0] * p3[1] - p2[1] * p3[0]);
	return 0.5f * sqrt((det1 * det1) + (det2 * det2) + (det3 * det3));
}

gVector4 Render::getPixelPoint(int x, int y)
{
	return basisM + ((((float)(2 * x) / (float)((width * pow(2, antiAliasingFactor)) - 1)) - 1) * basisH) + ((((float)(2 * y) / (float)((height * pow(2, antiAliasingFactor)) - 1)) - 1) * basisV);
}

gVector4 Render::getPixelRay(int x, int y)
{
	return (getPixelPoint(x, y) - eye).getNormalized();
}

float Render::Test_RaySphereIntersect(const gVector4 &P0, const gVector4 &V0, const gMatrix4 &T)
{
	gVector4 P1 = T.inverse() * P0;
	gMatrix4 T_Star = T;
	T_Star[0][3] = 0;
	T_Star[1][3] = 0;
	T_Star[2][3] = 0;
	gVector4 V = T_Star.inverse() * V0.getNormalized();

	float a = V * V;
	float b = (2 * V) * (P1 - gVector4(0, 0, 0, 1));
	float c = pow((P1 - gVector4(0, 0, 0, 1)).length(), 2) - 1;
	float t1 = (-b + sqrt((b * b) - (4 * a * c))) / (2 * a);
	float t2 = (-b - sqrt((b * b) - (4 * a * c))) / (2 * a);

	if (t1 > 0)
	{
		if (t2 > 0)
		{
			return std::min(t1, t2);
		}
		return t1;
	}
	else if (t2 > 0)
	{
		return t2;
	}
	return -1;
}

float Render::Test_RayPolyIntersect(const gVector4 &P0, const gVector4 &V0, const gVector4 &p1, const gVector4 &p2, const gVector4 &p3, const gMatrix4 &T)
{
	gVector4 P1 = T.inverse() * P0;
	gMatrix4 T_Star = T;
	T_Star[0][3] = 0;
	T_Star[1][3] = 0;
	T_Star[2][3] = 0;
	gVector4 V = T_Star.inverse() * V0;

	gVector4 v1 = p2 - p1;
	gVector4 v2 = p3 - p1;
	gVector4 N = (v1 % v2).getNormalized();

	float t = (N * (p1 - P1)) / (N * V);

	if (t >= 0)
	{
		gVector4 p = P1 + (t * V);
		float det1 = p1[1] * (p2[2] - p3[2]) - p1[2] * (p2[1] - p3[1]) + (p2[1] * p3[2] - p2[2] * p3[1]);
		float det2 = p1[2] * (p2[0] - p3[0]) - p1[0] * (p2[2] - p3[2]) + (p2[2] * p3[0] - p2[0] * p3[2]);
		float det3 = p1[0] * (p2[1] - p3[1]) - p1[1] * (p2[0] - p3[0]) + (p2[0] * p3[1] - p2[1] * p3[0]);
		float S = 0.5f * sqrt((det1 * det1) + (det2 * det2) + (det3 * det3));

		det1 = p[1] * (p2[2] - p3[2]) - p[2] * (p2[1] - p3[1]) + (p2[1] * p3[2] - p2[2] * p3[1]);
		det2 = p[2] * (p2[0] - p3[0]) - p[0] * (p2[2] - p3[2]) + (p2[2] * p3[0] - p2[0] * p3[2]);
		det3 = p[0] * (p2[1] - p3[1]) - p[1] * (p2[0] - p3[0]) + (p2[0] * p3[1] - p2[1] * p3[0]);
		float S1 = (0.5f * sqrt((det1 * det1) + (det2 * det2) + (det3 * det3))) / S;

		if (0 <= S1 && S1 <= 1)
		{
			det1 = p[1] * (p3[2] - p1[2]) - p[2] * (p3[1] - p1[1]) + (p3[1] * p1[2] - p3[2] * p1[1]);
			det2 = p[2] * (p3[0] - p1[0]) - p[0] * (p3[2] - p1[2]) + (p3[2] * p1[0] - p3[0] * p1[2]);
			det3 = p[0] * (p3[1] - p1[1]) - p[1] * (p3[0] - p1[0]) + (p3[0] * p1[1] - p3[1] * p1[0]);
			float S2 = (0.5f * sqrt((det1 * det1) + (det2 * det2) + (det3 * det3))) / S;

			if (0 <= S2 && S2 <= 1)
			{
				det1 = p[1] * (p1[2] - p2[2]) - p[2] * (p1[1] - p2[1]) + (p1[1] * p2[2] - p1[2] * p2[1]);
				det2 = p[2] * (p1[0] - p2[0]) - p[0] * (p1[2] - p2[2]) + (p1[2] * p2[0] - p1[0] * p2[2]);
				det3 = p[0] * (p1[1] - p2[1]) - p[1] * (p1[0] - p2[0]) + (p1[0] * p2[1] - p1[1] * p2[0]);
				float S3 = (0.5f * sqrt((det1 * det1) + (det2 * det2) + (det3 * det3))) / S;

				if (0 <= S3 && S3 <= 1 && approxEquals(S1 + S2 + S3, 1))
				{
					return t;
				}
			}
		}
	}
	return -1;
}

float Render::Test_RayCubeIntersect(const gVector4 &P0, const gVector4 &V0, const gMatrix4 &T)
{
	gVector4 P1 = T.inverse() * P0;
	gMatrix4 T_Star = T;
	T_Star[0][3] = 0;
	T_Star[1][3] = 0;
	T_Star[2][3] = 0;
	gVector4 V = T_Star.inverse() * V0;

	float xMin = (-0.5f - P1[0]) / V[0];
	float xMax = (0.5f - P1[0]) / V[0];

	if (xMin > xMax)
	{
		float temp = xMax;
		xMax = xMin;
		xMin = temp;
	}

	float yMin = (-0.5f - P1[1]) / V[1];
	float yMax = (0.5f - P1[1]) / V[1];

	if (yMin > yMax)
	{
		float temp = yMax;
		yMax = yMin;
		yMin = temp;
	}

	if ((xMin > yMax) || (yMin > xMax))
	{
		return -1;
	}

	if (yMin > xMin)
	{
		xMin = yMin;
	}
	if (yMax < xMax)
	{
		xMax = yMax;
	}

	float zMin = (-0.5f - P1[2]) / V[2];
	float zMax = (0.5f - P1[2]) / V[2];

	if (zMin > zMax)
	{
		float temp = zMax;
		zMax = zMin;
		zMin = temp;
	}

	if ((xMin > zMax) || (zMin > xMax))
	{
		return -1;
	}

	if (zMin > xMin)
	{
		xMin = zMin;
	}
	if (zMax < xMax)
	{
		xMax = zMax;
	}

	return xMin;
}

bool Render::isShadowCasted(const int &geoIndex, const gVector4 &intersectionPoint)
{
	gVector4 ray = (lightPosition - intersectionPoint).getNormalized();

	for (auto iter = geometries.begin(); iter != geometries.end(); iter++)
	{
		if (iter - geometries.begin() == geoIndex)
		{
			continue;
		}
		switch (iter->type)
		{
		case geometry::SPHERE:
			if (Test_RaySphereIntersect(intersectionPoint, ray, iter->transformationMatrix) >= 0)
			{
				return true;
			}
			break;
		case geometry::TRIANGLE:
			if (Test_RayPolyIntersect(intersectionPoint, ray, triangleP1, triangleP2, triangleP3, iter->transformationMatrix) >= 0)
			{
				return true;
			}
			break;
		case geometry::CUBE:
			if (Test_RayCubeIntersect(intersectionPoint, ray, iter->transformationMatrix) >= 0)
			{
				return true;
			}
			break;
		}
	}
	return false;
}

gVector4 Render::rayTrace(const gVector4 &origin, const gVector4 &ray, const int &counter, const int &reflectedGeoIndex)
{
	if (counter > NUM_BOUNCES)
	{
		return backgroundColor;
	}
	//For each shape in the geometries list
	float minT = -1;
	int geoIndex = -1;
	for (auto iter = geometries.begin(); iter != geometries.end(); iter++)
	{
		if (iter - geometries.begin() == reflectedGeoIndex)
		{
			continue;
		}
		//Get the intersection points (tempT)
		float tempT = minT;
		switch (iter->type)
		{
		case geometry::SPHERE:
			tempT = Test_RaySphereIntersect(origin, ray, iter->transformationMatrix);
			break;
		case geometry::TRIANGLE:
			tempT = Test_RayPolyIntersect(origin, ray, triangleP1, triangleP2, triangleP3, iter->transformationMatrix);
			break;
		case geometry::CUBE:
			tempT = Test_RayCubeIntersect(origin, ray, iter->transformationMatrix);
			break;
		}

		//Make sure that we have the closest intersection
		if ((minT < 0 && tempT >= 0) || (tempT >= 0 && tempT < minT))
		{
			minT = tempT;
			geoIndex = (int)(iter - geometries.begin());
		}
	}

	gVector4 color = backgroundColor;

	gVector4 ambient = gVector4(0, 0, 0, 1);

	gVector4 diffuse = gVector4(0, 0, 0, 1);
	gVector4 specular = gVector4(0, 0, 0, 1);
	if (minT >= 0)
	{
		ambient = geometries[geoIndex].materialColor ^ lightColor;
		gVector4 P1 = geometries[geoIndex].transformationMatrix.inverse() * origin;
		gMatrix4 T_Star = geometries[geoIndex].transformationMatrix;
		T_Star[0][3] = 0;
		T_Star[1][3] = 0;
		T_Star[2][3] = 0;
		gVector4 V = T_Star.inverse() * ray.getNormalized();

		gVector4 intersectionPoint = (ray * minT) + origin;
		gVector4 N = gVector4();
		switch (geometries[geoIndex].type)
		{
		case geometry::SPHERE:
			N = getSphereNormal(intersectionPoint);
			break;
		case geometry::TRIANGLE:
			N = getPolyNormal(intersectionPoint);
			break;
		case geometry::CUBE:
			gVector4 ip = (V * minT) + P1;
			N = geometries[geoIndex].transformationMatrix.inverse() * getCubeNormal(ip);
			break;
		}

		N.normalize();

		if (!isShadowCasted(geoIndex, intersectionPoint))
		{
			gVector4 L = (lightPosition - intersectionPoint).getNormalized();

			if (N * L >= 0)
			{
				diffuse = (N * L) * (geometries[geoIndex].materialColor ^ lightColor);
			}
			gVector4 specularV = (eye - intersectionPoint).getNormalized();
			gVector4 specularR = 2 * (L * N) * N - L;
			gVector4 specularH = (L + specularV).getNormalized();

			specular = pow(std::max(0.0f, std::min(specularV * specularR, 1.0f)), geometries[geoIndex].shininess) * lightColor;
		}

		ambient[3] = 1;
		diffuse[3] = 1;
		specular[3] = 1;
		color = 0.2f * ambient + 0.8f * diffuse + specular;
		color[3] = 1;

		gVector4 reflectedRay = (ray - 2 * (ray * N) * N).getNormalized();
		color = (1.0f - geometries[geoIndex].reflectivity) * color + geometries[geoIndex].reflectivity * rayTrace(intersectionPoint, reflectedRay, counter + 1, geoIndex);
		color[3] = 1;
	}

	color[0] = std::max(0.0f, std::min(color[0], 1.0f));
	color[1] = std::max(0.0f, std::min(color[1], 1.0f));
	color[2] = std::max(0.0f, std::min(color[2], 1.0f));
	color[3] = std::max(0.0f, std::min(color[3], 1.0f));

	return color;
}

void Render::generateImage(const std::string &FILENAME)
{
	BMP image;
	image.SetSize(width, height);
	//image.SetBitDepth(24);
	image.SetBitDepth(32);

	std::vector<std::vector<gVector4>> pixels;
	pixels.resize((unsigned __int64)(width * pow(2, antiAliasingFactor)));
	for (auto iter = pixels.begin(); iter != pixels.end(); iter++)
	{
		iter->resize((unsigned __int64)(height * pow(2, antiAliasingFactor)));
	}

	//For each pixel in the expanded colors array
	for (int y = 0; y < height * pow(2, antiAliasingFactor); y++)
	{
		for (int x = 0; x < width * pow(2, antiAliasingFactor); x++)
		{
			//Get the ray from the camera through the pixel specified
			gVector4 ray = getPixelRay(x, y);

			gVector4 color = rayTrace(eye, ray, 0, -1);

			pixels[x][(unsigned __int64)((height * pow(2, antiAliasingFactor)) - y - 1)] = color;
		}
	}

	for (int y = 0; y < height; y++)
	{
		for (int x = 0; x < width; x++)
		{
			int newX = (int)(x * pow(2, antiAliasingFactor));
			int newY = (int)(y * pow(2, antiAliasingFactor));
			gVector4 color;
			for (int b = newY; b < newY + pow(2, antiAliasingFactor); b++)
			{
				for (int a = newX; a < newX + pow(2, antiAliasingFactor); a++)
				{
					color += pixels[a][b];
				}
			}
			color /= (float)pow(pow(2, antiAliasingFactor), 2);
			image(x, y)->Red = (ebmpBYTE)(color[0] * 255);
			image(x, y)->Green = (ebmpBYTE)(color[1] * 255);
			image(x, y)->Blue = (ebmpBYTE)(color[2] * 255);
			image(x, y)->Alpha = (ebmpBYTE)(color[3] * 255);
		}
	}

	image.WriteToFile(FILENAME.c_str());
}

gVector4 Render::getSphereNormal(gVector4 poi)
{
	poi[3] = 0;
	return poi.getNormalized();
}
gVector4 Render::getPolyNormal(gVector4 poi)
{
	poi[3] = 0;
	return poi.getNormalized();
}
gVector4 Render::getCubeNormal(gVector4 poi)
{
	gVector4 N = gVector4();

	float x = 0.5f - abs(poi[0]);
	float y = 0.5f - abs(poi[1]);
	float z = 0.5f - abs(poi[2]);

	if (x < y && x < z)
	{
		N = gVector4(poi[0] / abs(poi[0]), 0, 0, 0);
	}
	else if (y < x && y < z)
	{
		N = gVector4(0, poi[1] / abs(poi[1]), 0, 0);
	}
	else if (z < x && z < y)
	{
		N = gVector4(0, 0, poi[2] / abs(poi[2]), 0);
	}
	return N;
}

gVector4 Render::getIntersectionPoint(gVector4 origin, gVector4 ray, float t)
{
	return (ray * t) + origin;
}