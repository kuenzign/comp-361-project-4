#include "gVector4.h"
#include <cmath>

bool gVector4::approxEquals(float a, float b)
{
	const float epsilon = 0.00001f;
	if (a == b) {
		return true;
	}
	else if(a * b == 0.0f) {
		return fabs(a - b) < epsilon * epsilon;
	}
	else {
		return fabs(a - b) / (fabs(a) + fabs(b)) < epsilon;
	}
}

gVector4::gVector4() {
	data[0] = 0.0f;
	data[1] = 0.0f;
	data[2] = 0.0f;
	data[3] = 0.0f;
}

gVector4::gVector4(float x, float y, float z, float w) {
	data[0] = x;
	data[1] = y;
	data[2] = z;
	data[3] = w;
}

gVector4::gVector4(const gVector4& other) {
	data[0] = other.data[0];
	data[1] = other.data[1];
	data[2] = other.data[2];
	data[3] = other.data[3];
}

float gVector4::operator[](unsigned int index) const {
	return data[index];
}

float& gVector4::operator[](unsigned int index) {
	return data[index];
}

float gVector4::length() const {
	return sqrtf(*this * *this);
}

void gVector4::normalize() {
	float magnitude = this->length();
	data[0] /= magnitude;
	data[1] /= magnitude;
	data[2] /= magnitude;
	data[3] /= magnitude;
}

gVector4 gVector4::getNormalized() const {
	float magnitude = this->length();
	return gVector4(data[0] / magnitude, data[1] / magnitude, data[2] / magnitude, data[3] / magnitude);
}

bool gVector4::operator==(const gVector4& v2) const {
	return approxEquals(data[0], v2.data[0]) && approxEquals(data[1], v2.data[1]) && approxEquals(data[2], v2.data[2]) && approxEquals(data[3], v2.data[3]);
}

bool gVector4::operator!=(const gVector4& v2) const {
	return !(*this == v2);
}

gVector4 gVector4::operator+(const gVector4& v2) const {
	return gVector4(data[0] + v2.data[0], data[1] + v2.data[1], data[2] + v2.data[2], data[3] + v2.data[3]);
}

gVector4& gVector4::operator+=(const gVector4& v2) {
	data[0] += v2.data[0];
	data[1] += v2.data[1];
	data[2] += v2.data[2];
	data[3] += v2.data[3];
	return *this;
}

gVector4 gVector4::operator-(const gVector4& v2) const {
	return gVector4(data[0] - v2.data[0], data[1] - v2.data[1], data[2] - v2.data[2], data[3] - v2.data[3]);
}

gVector4& gVector4::operator-=(const gVector4& v2) {
	data[0] -= v2.data[0];
	data[1] -= v2.data[1];
	data[2] -= v2.data[2];
	data[3] -= v2.data[3];
	return *this;
}

gVector4 gVector4::operator*(float c) const {
	return gVector4(data[0] * c, data[1] * c, data[2] * c, data[3] * c);
}

gVector4& gVector4::operator*=(float c) {
	data[0] *= c;
	data[1] *= c;
	data[2] *= c;
	data[3] *= c;
	return *this;
}

gVector4 gVector4::operator/(float c) const {
	return gVector4(data[0] / c, data[1] / c, data[2] / c, data[3] / c);
}

gVector4& gVector4::operator/=(float c) {
	data[0] /= c;
	data[1] /= c;
	data[2] /= c;
	data[3] /= c;
	return *this;
}

float gVector4::operator*(const gVector4& v2) const {
	return data[0] * v2.data[0] + data[1] * v2.data[1] + data[2] * v2.data[2] + data[3] * v2.data[3];
}

gVector4 gVector4::operator%(const gVector4& v2) const {
	return gVector4(data[1] * v2.data[2] - data[2] * v2.data[1], data[2] * v2.data[0] - data[0] * v2.data[2], data[0] * v2.data[1] - data[1] * v2.data[0], 0.0f);
}

gVector4 gVector4::operator^(const gVector4& v2) const {
	return gVector4(data[0] * v2.data[0], data[1] * v2.data[1], data[2] * v2.data[2], data[3] * v2.data[3]);
}

gVector4& gVector4::operator^=(const gVector4& v2) {
	data[0] *= v2.data[0];
	data[1] *= v2.data[1];
	data[2] *= v2.data[2];
	data[3] *= v2.data[3];
	return *this;
}

gVector4 operator*(float c, const gVector4& v) {
	return gVector4(v.data[0] * c, v.data[1] * c, v.data[2] * c, v.data[3] * c);
}