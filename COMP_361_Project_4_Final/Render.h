#include <string>
#include <math.h>
#include <algorithm>
#include <vector>
#include "EasyBMP.h"
#include "gMatrix4.h"
#include "gVector4.h"

#define PI 3.14159265
#define NUM_BOUNCES 3

class Render
{
private:
	int antiAliasingFactor;
	int width, height;
	float fovY, fovX, phi, theta;
	gVector4 eye, at, up, n, u, basisV, basisH, basisM, backgroundColor, lightPosition, lightColor;
	gVector4 triangleP1, triangleP2, triangleP3;

public:
	struct geometry
	{
		enum geoType
		{
			NOTHING,
			SPHERE,
			TRIANGLE,
			CUBE
		} type;
		gVector4 materialColor;
		gMatrix4 transformationMatrix;
		int shininess;
		float reflectivity;

		geometry(void) : type(NOTHING), materialColor(gVector4()), transformationMatrix(gMatrix4::identity()), shininess(1), reflectivity(0) {}
		geometry(geoType t, gVector4 mCol, gMatrix4 T, int a, float r) : type(t), materialColor(mCol), transformationMatrix(T), shininess(a), reflectivity(r) {}
	};
	std::vector<geometry> geometries;

	Render(gVector4 AT, gVector4 EYEPOS, gVector4 UP, gVector4 LCOL, gVector4 LPOS, gVector4 BCOL, float FOVY, int RESOx, int RESOy, int aaFactor);
	~Render(void);

	bool approxEquals(float a, float b);
	double triangeArea(gVector4 p1, gVector4 p2, gVector4 p3);
	gVector4 getPixelPoint(int x, int y);
	gVector4 getPixelRay(int x, int y);

	// Tries to find the intersection of a ray and a sphere.
	// P0 is the position from which the ray emanates; V0 is the
	// direction of the ray.
	// T is the transformation matrix of the sphere.
	// This function should return the smallest positive t-value of the intersection
	// (a point such that P0+t*V0 intersects the sphere), or -1 if there is no
	// intersection.
	float Test_RaySphereIntersect(const gVector4 &P0, const gVector4 &V0, const gMatrix4 &T);

	// Tries to find the intersection of a ray and a triangle.
	// This is just like the above function, but it intersects the ray with a
	// triangle instead. The parameters p1, p2, and p3 specify the three
	// points of the triangle, in object space.
	float Test_RayPolyIntersect(const gVector4 &P0, const gVector4 &V0, const gVector4 &p1, const gVector4 &p2, const gVector4 &p3, const gMatrix4 &T);

	// This is just like Test_RaySphereIntersect, but with a unit cube instead of a
	// sphere. A unit cube extends from -0.5 to 0.5 in all axes.
	float Test_RayCubeIntersect(const gVector4 &P0, const gVector4 &V0, const gMatrix4 &T);

	bool isShadowCasted(const int &geoIndex, const gVector4 &intersectionPoint);
	gVector4 rayTrace(const gVector4 &origin, const gVector4 &ray, const int &counter, const int &reflectedGeoIndex);

	void generateImage(const std::string &FILENAME);

	gVector4 getSphereNormal(gVector4 t);
	gVector4 getPolyNormal(gVector4 t);
	gVector4 getCubeNormal(gVector4 t);
	gVector4 getIntersectionPoint(gVector4 origin, gVector4 ray, float t);
};
