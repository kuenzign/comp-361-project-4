//Base code written by Jan Allbeck, Chris Czyzewicz, and Cory Boatright
//University of Pennsylvania

//modified March 2, 2017 at Grove City College

#pragma once

class gVector4{
private:
	float data[4];
	static bool approxEquals(float, float);

public:
	///----------------------------------------------------------------------
	/// Constructors
	///----------------------------------------------------------------------
	gVector4(void);
	gVector4(float, float, float, float);
	gVector4(const gVector4&);

	///----------------------------------------------------------------------
	/// Getters/Setters
	///----------------------------------------------------------------------
	/// Returns the value at index
	float operator[](unsigned int) const;

	/// Returns a reference to the value at index
	float& operator[](unsigned int);

	///----------------------------------------------------------------------
	/// Vector Operations
	///----------------------------------------------------------------------
	/// Returns the geometric length of the vector
	float length(void) const;

	void normalize(void);

	gVector4 getNormalized(void) const;

	///----------------------------------------------------------------------
	/// Operator Functions
	///----------------------------------------------------------------------
	/// Checks if v1 == v2
	bool operator==(const gVector4&) const;

	/// Checks if v1 != v2
	bool operator!=(const gVector4&) const;

	/// Vector Addition (v1 + v2)
	gVector4 operator+(const gVector4&) const;

	/// Destructive vector addition: remember to return *this
	gVector4& operator+=(const gVector4&);

	/// Vector Subtraction (v1 - v2)
	gVector4 operator-(const gVector4&) const;

	/// Destructive vector subtraction: remember to return *this
	gVector4& operator-=(const gVector4&);

	/// Scalar Multiplication (v * c)
	gVector4 operator*(float) const;

	/// Destructive scalar multiplication: remember to return *this
	gVector4& operator*=(float);

	/// Scalar Multiplication (c * v)
	friend gVector4 operator*(float, const gVector4&);

	/// Scalar Division (v/c)
	gVector4 operator/(float) const;

	/// Destructive scalar division: remember to return *this
	gVector4& operator/=(float);

	/// Dot Product (v1 * v2)
	float operator*(const gVector4&) const;

	/// Cross Product (v1 % v2)
	gVector4 operator%(const gVector4&) const;

	/// Component-wise Product (v1 ^ v2) -- note, because of operator precedence, always use parentheses with this operator
	/// example: v3 = c * (v1 ^ v2)
	gVector4 operator^(const gVector4&) const;

	/// Destructive component-wise product: remember to return *this
	gVector4& operator^=(const gVector4&);
};