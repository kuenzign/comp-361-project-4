/**
This testing framework has been developed/overhauled over time, primarily by:
Chris Czyzewicz
Ben Sunshine-Hill
Cory Boatright

While the three were PhD students at UPenn (through 2013).  This particular version has some
modifications by Cory since joining the faculty of Grove City College.

Last revised 4/10/2017
*/

#include "Render.h"
#include <iostream>

int main(int argc, char **argv)
{
	Render r1 = Render(gVector4(0, 0, 0, 1), gVector4(-3, 0, 3, 1), gVector4(0, 1, 0, 0), gVector4(1, 1, 1, 1), gVector4(5, 5, 5, 1), gVector4(0, 0, 0, 0), 90, 640, 480, 0);
	Render r2 = Render(gVector4(0, 0, 0, 1), gVector4(-3, 0, 3, 1), gVector4(0, 1, 0, 0), gVector4(1, 1, 1, 1), gVector4(5, 5, 5, 1), gVector4(0, 0, 0, 0), 90, 640, 480, 1);
	Render r3 = Render(gVector4(0, 0, 0, 1), gVector4(-3, 0, 3, 1), gVector4(0, 1, 0, 0), gVector4(1, 1, 1, 1), gVector4(5, 5, 5, 1), gVector4(0, 0, 0, 0), 90, 640, 480, 2);
	gMatrix4 matrix = gMatrix4::identity() * gMatrix4::translate3D(0, 1, 0) * gMatrix4::rotateZ(45.0f) * gMatrix4::scale3D(1.5, 1, 1);

	Render::geometry geo1 = Render::geometry(Render::geometry::SPHERE, gVector4(1, 0, 0, 1), gMatrix4::translate3D(0, 0, 0), 64, 0.1f);
	Render::geometry geo2 = Render::geometry(Render::geometry::CUBE, gVector4(1, 1, 0, 1), gMatrix4::translate3D(0, -2, 0) * gMatrix4::scale3D(10, 1, 10), 256, 0.2f);
	Render::geometry geo3 = Render::geometry(Render::geometry::TRIANGLE, gVector4(1, 0, 1, 1), gMatrix4::translate3D(0, 2, 1.5) * gMatrix4::rotateX(90), 256, 0.2f);

	r1.geometries.push_back(geo1);
	r1.geometries.push_back(geo2);
	r1.geometries.push_back(geo3);
	r2.geometries.push_back(geo1);
	r2.geometries.push_back(geo2);
	r2.geometries.push_back(geo3);
	r3.geometries.push_back(geo1);
	r3.geometries.push_back(geo2);
	r3.geometries.push_back(geo3);

	r1.generateImage("scene1.bmp");
	r2.generateImage("scene2.bmp");
	r3.generateImage("scene3.bmp");

	return 0;
}