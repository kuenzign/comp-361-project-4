#include "gMatrix4.h"
#include <cmath>

const float gMatrix4::PI_RAD = 0.0174532925199432957692369768489f;

gMatrix4::gMatrix4() {
	data[0] = gVector4(0.0f, 0.0f, 0.0f, 0.0f);
	data[1] = gVector4(0.0f, 0.0f, 0.0f, 0.0f);
	data[2] = gVector4(0.0f, 0.0f, 0.0f, 0.0f);
	data[3] = gVector4(0.0f, 0.0f, 0.0f, 0.0f);
}

gMatrix4::gMatrix4(const gVector4& row0, const gVector4& row1, const gVector4& row2, const gVector4& row3) {
	data[0] = row0;
	data[1] = row1;
	data[2] = row2;
	data[3] = row3;
}

gMatrix4::gMatrix4(const gMatrix4& other) {
	data[0] = other.data[0];
	data[1] = other.data[1];
	data[2] = other.data[2];
	data[3] = other.data[3];
}

gVector4 gMatrix4::operator[](unsigned int index) const {
	return data[index];
}

gVector4& gMatrix4::operator[](unsigned int index) {
	return data[index];
}

gVector4 gMatrix4::getColumn(unsigned int index) const {
	return gVector4(data[0][index], data[1][index], data[2][index], data[3][index]);
}

gMatrix4 gMatrix4::transpose() const {
	return gMatrix4(getColumn(0), getColumn(1), getColumn(2), getColumn(3));
}

gMatrix4 gMatrix4::inverse() const {
	float factor0 = data[2][2] * data[3][3] - data[3][2] * data[2][3];
	float factor1 = data[2][1] * data[3][3] - data[3][1] * data[2][3];
	float factor2 = data[2][1] * data[3][2] - data[3][1] * data[2][2];
	float factor3 = data[2][0] * data[3][3] - data[3][0] * data[2][3];
	float factor4 = data[2][0] * data[3][2] - data[3][0] * data[2][2];
	float factor5 = data[2][0] * data[3][1] - data[3][0] * data[2][1];
	float factor6 = data[1][2] * data[3][3] - data[3][2] * data[1][3];
	float factor7 = data[1][1] * data[3][3] - data[3][1] * data[1][3];
	float factor8 = data[1][1] * data[3][2] - data[3][1] * data[1][2];
	float factor9 = data[1][0] * data[3][3] - data[3][0] * data[1][3];
	float factor10 = data[1][0] * data[3][2] - data[3][0] * data[1][2];
	float factor11 = data[1][1] * data[3][3] - data[3][1] * data[1][3];
	float factor12 = data[1][0] * data[3][1] - data[3][0] * data[1][1];
	float factor13 = data[1][2] * data[2][3] - data[2][2] * data[1][3];
	float factor14 = data[1][1] * data[2][3] - data[2][1] * data[1][3];
	float factor15 = data[1][1] * data[2][2] - data[2][1] * data[1][2];
	float factor16 = data[1][0] * data[2][3] - data[2][0] * data[1][3];
	float factor17 = data[1][0] * data[2][2] - data[2][0] * data[1][2];
	float factor18 = data[1][0] * data[2][1] - data[2][0] * data[1][1];

	gMatrix4 inverse;
	inverse[0][0] = (data[1][1] * factor0 - data[1][2] * factor1 + data[1][3] * factor2);
	inverse[1][0] = -(data[1][0] * factor0 - data[1][2] * factor3 + data[1][3] * factor4);
	inverse[2][0] = (data[1][0] * factor1 - data[1][1] * factor3 + data[1][3] * factor5);
	inverse[3][0] = -(data[1][0] * factor2 - data[1][1] * factor4 + data[1][2] * factor5);

	inverse[0][1] = -(data[0][1] * factor0 - data[0][2] * factor1 + data[0][3] * factor2);
	inverse[1][1] = (data[0][0] * factor0 - data[0][2] * factor3 + data[0][3] * factor4);
	inverse[2][1] = -(data[0][0] * factor1 - data[0][1] * factor3 + data[0][3] * factor5);
	inverse[3][1] = (data[0][0] * factor2 - data[0][1] * factor4 + data[0][2] * factor5);

	inverse[0][2] = (data[0][1] * factor6 - data[0][2] * factor7 + data[0][3] * factor8);
	inverse[1][2] = -(data[0][0] * factor6 - data[0][2] * factor9 + data[0][3] * factor10);
	inverse[2][2] = (data[0][0] * factor11 - data[0][1] * factor9 + data[0][3] * factor12);
	inverse[3][2] = -(data[0][0] * factor8 - data[0][1] * factor10 + data[0][2] * factor12);

	inverse[0][3] = -(data[0][1] * factor13 - data[0][2] * factor14 + data[0][3] * factor15);
	inverse[1][3] = (data[0][0] * factor13 - data[0][2] * factor16 + data[0][3] * factor17);
	inverse[2][3] = -(data[0][0] * factor14 - data[0][1] * factor16 + data[0][3] * factor18);
	inverse[3][3] = (data[0][0] * factor15 - data[0][1] * factor17 + data[0][2] * factor18);

	float determinant = data[0][0] * inverse[0][0] + data[1][0] * inverse[0][1]	+ data[2][0] * inverse[0][2] + data[3][0] * inverse[0][3];

	inverse = gMatrix4(inverse[0]/determinant, inverse[1]/determinant, inverse[2]/determinant, inverse[3]/determinant);

	return inverse;
}

gMatrix4 gMatrix4::rotateX(float angle) {
	angle *= PI_RAD;
	float c = cosf(angle);
	float s = sinf(angle);
	return gMatrix4(gVector4(1.0f, 0.0f, 0.0f, 0.0f),
		gVector4(0.0f, c, -s, 0.0f),
		gVector4(0.0f, s, c, 0.0f),
		gVector4(0.0f, 0.0f, 0.0f, 1.0f));
}

gMatrix4 gMatrix4::rotateY(float angle) {
	angle *= PI_RAD;
	float c = cosf(angle);
	float s = sinf(angle);
	return gMatrix4(gVector4(c, 0.0f, s, 0.0f),
		gVector4(0.0f, 1.0f, 0.0f, 0.0f),
		gVector4(-s, 0.0f, c, 0.0f),
		gVector4(0.0f, 0.0f, 0.0f, 1.0f));
}

gMatrix4 gMatrix4::rotateZ(float angle) {
	angle *= PI_RAD;
	float c = cosf(angle);
	float s = sinf(angle);
	return gMatrix4(gVector4(c, -s, 0.0f, 0.0f),
		gVector4(s, c, 0.0f, 0.0f),
		gVector4(0.0f, 0.0f, 1.0f, 0.0f),
		gVector4(0.0f, 0.0f, 0.0f, 1.0f));
}

gMatrix4 gMatrix4::translate3D(float x, float y, float z) {
	return gMatrix4(gVector4(1.0f, 0.0f, 0.0f, x),
		gVector4(0.0f, 1.0f, 0.0f, y),
		gVector4(0.0f, 0.0f, 1.0f, z),
		gVector4(0.0f, 0.0f, 0.0f, 1.0f));
}

gMatrix4 gMatrix4::scale3D(float x, float y, float z) {
	return gMatrix4(gVector4(x, 0.0f, 0.0f, 0.0f),
		gVector4(0.0f, y, 0.0f, 0.0f),
		gVector4(0.0f, 0.0f, z, 0.0f),
		gVector4(0.0f, 0.0f, 0.0f, 1.0f));
}

gMatrix4 gMatrix4::identity() {
	return gMatrix4(gVector4(1.0f, 0.0f, 0.0f, 0.0f),
		gVector4(0.0f, 1.0f, 0.0f, 0.0f),
		gVector4(0.0f, 0.0f, 1.0f, 0.0f),
		gVector4(0.0f, 0.0f, 0.0f, 1.0f));
}

bool gMatrix4::operator==(const gMatrix4& m2) const {
	return data[0] == m2.data[0] && data[1] == m2.data[2] && data[2] == m2.data[2] && data[3] == m2.data[3];
}

bool gMatrix4::operator!=(const gMatrix4& m2) const {
	return !(*this == m2);
}

gMatrix4 gMatrix4::operator+(const gMatrix4& m2) const {
	return gMatrix4(data[0] + m2.data[0], data[1] + m2.data[1], data[2] + m2.data[2], data[3] + m2.data[3]);
}

gMatrix4& gMatrix4::operator+=(const gMatrix4& m2) {
	data[0] += m2.data[0];
	data[1] += m2.data[1];
	data[2] += m2.data[2];
	data[3] += m2.data[3];
	return *this;
}

gMatrix4 gMatrix4::operator-(const gMatrix4& m2) const {
	return gMatrix4(data[0] - m2.data[0], data[1] - m2.data[1], data[2] - m2.data[2], data[3] - m2.data[3]);
}

gMatrix4 & gMatrix4::operator-=(const gMatrix4& m2) {
	data[0] -= m2.data[0];
	data[1] -= m2.data[1];
	data[2] -= m2.data[2];
	data[3] -= m2.data[3];
	return *this;
}

gMatrix4 gMatrix4::operator*(const gMatrix4& m2) const {
	gMatrix4 other = m2.transpose();	//gets the columns, which can be indexed with regular operator[]
	gMatrix4 result(*this * other[0], *this * other[1], *this * other[2], *this * other[3]);
	return result.transpose();
}

gMatrix4& gMatrix4::operator*(const gMatrix4& m2) {
	gMatrix4 other = m2.transpose();
	gMatrix4 result(*this * other[0], *this * other[1], *this * other[2], *this * other[3]);
	data[0] = result.getColumn(0);
	data[1] = result.getColumn(1);
	data[2] = result.getColumn(2);
	data[3] = result.getColumn(3);
	return *this;
}

gVector4 gMatrix4::operator*(const gVector4& v) const {
	return gVector4(data[0] * v, data[1] * v, data[2] * v, data[3] * v);
}