To create your own scenes, go to main.cpp.
You must create a Render object using the the parameters as follows:
Render(gVector4 AT, gVector4 EYEPOS, gVector4 UP, gVector4 LCOL, gVector4 LPOS, gVector4 BCOL, float FOVY, int RESOx, int RESOy, int aaFactor)
	AT: the point the camera is looking at
	EYEPOS: the location of the camera
	UP: the vector pointing up in relation to the camera
	LCOL: the light color
	LPOS: the light position
	BCOL: the background color
	FOVY: the field of view in the Y axis (if you were expecting to use 45 degrees, use 90 instead)
	RESOx: the width of the bitmap
	RESOy: the height of the bitmap
	aaFactor: the anti-aliasing factor (use 0 for no AA, 1 for medium AA, and 2+ for best AA)

Then you can create geometry objects like this:
Render::geometry(geoType t, gVector4 mCol, gMatrix4 T, int a, float r)
	t: the enum for the type of geometry it is (SPHERE, TRIANGLE, CUBE)
	mCol: the material color of the geometry
	T: the transformation matrix
	a: the shininess exponent (powers of 2: 1, 2, 4, 8, ..., 64, 128, ...)
	r: the reflective component (a decimal between 0 and 1)

Then you can add your geometry object to your Render object:
render.geometries.push_back(geo)

Then you can render the image by passing in the filename:
render.generateImage("filename.bmp")